package com.zuitt;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        Contact firstContact = new Contact();
        firstContact.setName("Michael Sagabaen");
        firstContact.addNumber("09999999999");
        firstContact.addNumber("09888888888");
        firstContact.addAddress("Quezon City");
        firstContact.addAddress("Manila");

        Contact secondContact = new Contact();
        secondContact.setName("Lloyd Sagabaen");
        secondContact.addNumber("09777777777");
        secondContact.addNumber("09666666666");
        secondContact.addAddress("Pasay City");
        secondContact.addAddress("Mandaluyong");

        phonebook.setContacts(firstContact);
        phonebook.setContacts(secondContact);

        if (phonebook.getContact() == null) {
            System.out.println("The phonebook is empty!");
        } else {
            for (var i = 0; i < phonebook.getContact().toArray().length; i++ ) {
                System.out.println(phonebook.getContact().get(i).getName());
                System.out.println("--------------------");
                System.out.println(phonebook.getContact().get(i).getName() + " has the following registered numbers:");
                for(var j = 0; j < phonebook.getContact().get(i).getNumbers().toArray().length; j++){
                    System.out.println(phonebook.getContact().get(i).getNumbers().toArray()[j]);
                }
                System.out.println("---------------------------------");
                System.out.println(phonebook.getContact().get(i).getName() + " has the following registered addresses:");
                for(var k = 0; k < phonebook.getContact().get(i).getAddresses().toArray().length; k++){
                    System.out.println(phonebook.getContact().get(i).getAddresses().toArray()[k]);
                }
                System.out.println("=================================");
            }
        }
    }
}
