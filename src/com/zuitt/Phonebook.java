package com.zuitt;

import java.util.ArrayList;

public class Phonebook extends Contact {

    // instance variables
    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    // constructor
    public Phonebook() {
        super();
    }

    // parametrized constructor
    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    // getters
    public ArrayList<Contact> getContact() {
        return contacts;
    }


    // setters
    public void setContacts(Contact contact) {
        contacts.add(contact);
    }


}
